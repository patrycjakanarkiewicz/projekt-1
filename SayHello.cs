using System;

namespace group
{
    class Conflict
    {
        static void Main(string[] args)
        {
            SayHello("Patrycja");
        }

        static void SayHello(string name, string surname, string sex)
        {
            Console.WriteLine("Dzień Dobry " + name + " " + surname + " " + sex);
        }
    }
}
